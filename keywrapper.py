#!/usr/bin/env python3
#
# Nitrokeywrapper
# Copyright (c) 2020 Marcus Hoffmann <bubu@bubu1.eu>
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, see <http://www.gnu.org/licenses/>.

import os
import rsa
import jks
import sys
from argparse import ArgumentParser

from lib import dkek
from lib.wrapped_key import build_wrapped_key


parser = ArgumentParser()
parser.add_argument("--config-py", default=None,
                    help="Path to F-Droid config.py to read values from")
parser.add_argument("--dkek-shares", type=int, default=1,
                    help="Define the number of DKEK shares to use for recreating the DKEK")
parser.add_argument("--dkek-share-path", default="dkek-share-1.pbe",
                    help="Path to the DKEK share file")
parser.add_argument("--dkek-share-password-var", default=None,
                    help="The env var that contains the password to unlock the DKEK share file")
parser.add_argument("--dkek-share-first-keyid", type=int, default=0,
                    help="The starting value for the key ID counter.")
options = parser.parse_args()

config = dict()
if options.config_py:
    from fdroidserver import common
    config = common.read_config(options)
    for k in ('keystorepass', 'keystore'):
        if k not in config:
            print('ERROR: %s must be in config.py!' % k)
            sys.exit(1)
    JKS_PWD = config['keystorepass']
    JKS_PATH = config['keystore']
    NUMBER_OF_DKEK_SHARES = options.dkek_shares
    if options.dkek_share_password_var:
        DKEK_PWDS = [os.getenv(options.dkek_share_password_var)]
    else:
        DKEK_PWDS = [config['keystorepass']]
    DKEK_SHAREPATHS = [options.dkek_share_path]
else:
    JKS_PWD = "SECUREPASSWORD"
    JKS_PATH = "/path/to/keystore.jks"
    NUMBER_OF_DKEK_SHARES = 1
    DKEK_PWDS = ["ANOTHERPWD"]
    DKEK_SHAREPATHS = ["/path/to/share1"]

if not JKS_PATH.endswith('.jks'):
    print('WARNING: this script only works with JKS keystores!')
    print('To convert, run:')
    print('keytool -importkeystore -srckeystore {p12} -srcstoretype pkcs12 -srcalias {alias} -srcstorepass:env keystorepass -destkeystore {jks} -deststoretype jks -destalias {alias} -deststorepass:env keystorepass\n'
          .format(p12=JKS_PATH, jks=JKS_PATH[:-3] + 'jks', alias=config.get('repo_keyalias')))

ks = jks.KeyStore.load(JKS_PATH, JKS_PWD)
d = dkek.DKEK()
for i in range(NUMBER_OF_DKEK_SHARES):
    print("Processing DKEK share #%s of %s" % (str(i + 1), NUMBER_OF_DKEK_SHARES))
    with open(DKEK_SHAREPATHS[i], 'rb') as f:
        d.import_dkek_share(dkek.decrypt_keyshare(f.read(), DKEK_PWDS[i]))

print("Computed DKEK KCV: %s" % d.get_kcv().hex())
i = options.dkek_share_first_keyid
for alias, pk in ks.private_keys.items():
    print("Wrapping key: %s" % pk.alias)
    cert = pk.cert_chain[0][1]
    privkey = rsa.PrivateKey.load_pkcs1(pk.pkey, format="DER")
    pubkey = rsa.PublicKey(privkey.n, privkey.e)

    encoded_key = d.encode_key(privkey, pubkey)
    # 0<=keyid<256, it also needs to be unique for 2 keys to be usable at the same time.
    # for now we blindly cycle through them.
    wrapped_key = build_wrapped_key(encoded_key, privkey.n.bit_length(), alias, cert, keyid=i % 256)

    with open(alias + "_wrapped.bin", "wb") as f:
        f.write(wrapped_key)
    i += 1
