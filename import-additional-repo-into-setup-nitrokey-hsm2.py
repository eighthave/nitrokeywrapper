#!/usr/bin/env python3

# If the repo's keystore is a PKCS12 aka .p12 keystore, it needs to be
# converted to the old JKS format for this script:
#
#  keytool -importkeystore \
#    -srckeystore keystore.p12 -srcstoretype pkcs12 -srcalias FOO -srcstorepass:env keystorepass \
#    -destkeystore keystore.jks -deststoretype jks -destalias FOO -deststorepass:env keystorepass

import glob
import os
import re
import subprocess
import sys
from fdroidserver import common
from packaging import version
from random import randrange


def get_random_name():
    ret = ''
    for i in range(32):
        ret += chr(randrange(25) + 65)
    return ret


if len(sys.argv) != 2:
    print('ERROR: this requires a single argument: the starting key ID!')
    print('Try `nitrotool identify | grep ID` to see what is in used')
    sys.exit(1)

if 'PIN' not in os.environ or 'SO_PIN' not in os.environ:
    print('ERROR: the new PIN values must be set in the env vars PIN and SO_PIN '
          + '(this script will set the PINs to these new values)!')
    sys.exit(1)

if not os.path.exists('config.py'):
    print('ERROR: this must be run in the base of the fdroid repo, where config.py is!')
    sys.exit(1)

options = type('', (), {})()
config = common.read_config(options)

for k in ('keystorepass', 'keystore'):
    if k not in config:
        print('ERROR: %s must be in config.py!' % k)
        sys.exit(1)

output = subprocess.check_output(['sc-hsm-tool'], universal_newlines=True)
m = re.match(r'^Version\s*:\s([0-9]+\.[0-9.]+)', output)
if m:
    if version.parse('3.0') > version.parse(m.group(1)):
        print('ERROR: NitroKey HSM must have firmware version 3.0 or higher '
              + '(only NitroKey HSM2 hardware can handle 4096 bit RSA keys)!')
        #sys.exit(1)

dkek_share_path = '/home/geheimnis/guardianproject.info/nitrokey-hsm2-dkek-share-1.pbe'
varname = get_random_name()
keystorepass_file = os.path.join(os.path.dirname(dkek_share_path), 'config.py')
keystorepass = None
with open(keystorepass_file) as fp:
    m = re.search(r'keystorepass *= *"(.+)"', fp.read())
    if m:
        keystorepass = m.group(1)
if not keystorepass:
    print('ERROR: "keystorepass" env var not properly set!')
    print('This should come from the config.py from ', keystorepass_file)
    sys.exit(1)
env = {
    varname: keystorepass,
    'HOME': os.getenv('HOME'),
}
keywrapper = os.path.join(os.path.dirname(__file__), 'keywrapper.py')
first_keyid = 200
print('\n---------------\nRunning %s:' % keywrapper)
p = subprocess.check_call([keywrapper,
                           '--config-py', 'config.py',
                           '--dkek-shares', '1',
                           '--dkek-share-path', dkek_share_path,
                           '--dkek-share-first-keyid', str(first_keyid),
                           '--dkek-share-password-var', varname],
                          env=env)

print('\n---------------\nUnwrapping and importing keys:')
# -protected makes it not prompt for a PIN, this is a query of public info
p = subprocess.run(['keytool', '-list', '-keystore', config['keystore'], '-protected'],
                   env=env, stdout=subprocess.PIPE, universal_newlines=True)
if p.returncode != 0:
    print('ERROR: ', p.returncode, '\n', p.stdout)
    sys.exit(1)
pat = re.compile(r'([^\n,]+), .+, PrivateKeyEntry,.*')
i = first_keyid
for m in pat.findall(p.stdout):
    print('\n\nimporting %s:' % m)
    subprocess.check_call(['sc-hsm-tool', '--pin', 'env:PIN', '--force',
                           '--unwrap-key', m + '_wrapped.bin', '--key-reference', str(i)])
    i += 1


if not os.path.exists('opensc-fdroid.cfg'):
    print('\n---------------\nWriting opensc-fdroid.cfg:')
    if os.path.exists('/usr/lib/opensc-pkcs11.so'):
        opensc_so = '/usr/lib/opensc-pkcs11.so'
    elif os.path.exists('/usr/lib64/opensc-pkcs11.so'):
        opensc_so = '/usr/lib64/opensc-pkcs11.so'
    else:
        files = glob.glob('/usr/lib/' + os.uname()[4] + '-*-gnu/opensc-pkcs11.so')
        if len(files) > 0:
            opensc_so = files[0]
        else:
            opensc_so = '/usr/lib/opensc-pkcs11.so'
            logging.warning('No OpenSC PKCS#11 module found, '
                            + 'install OpenSC then edit "opensc-fdroid.cfg"!')
    with open('opensc-fdroid.cfg', 'w') as f:
        f.write('name = OpenSC\nlibrary = ')
        f.write(opensc_so)
        f.write('\n')

print('\n---------------\nWriting suggested values to config.py:')
os.chmod('config.py', 0o600)
with open('config.py', 'a') as fp:
    fp.write('''# Suggested values with NitroKey HSM2\n''')
    fp.write('''#keystorepass = os.getenv('PIN')\n''')
    fp.write('''#keystore = "NONE"\n''')
