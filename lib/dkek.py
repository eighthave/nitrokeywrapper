# Nitrokeywrapper
# Copyright (c) 2020 Marcus Hoffmann <bubu@bubu1.eu>
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, see <http://www.gnu.org/licenses/>.
#
# Original javascript version:
# SmartCard-HSM Support Scripts
# Copyright (c) 2011-2012 CardContact Software & System Consulting
# Andreas Schwier, 32429 Minden, Germany (www.cardcontact.de)

import hashlib
import struct
import secrets
import rsa
import math
from Cryptodome.Cipher import AES
from Cryptodome.Hash import CMAC


def _get_number_of_bytes(i):
    return math.ceil(i.bit_length() / 8)


def _derive_dkek_share_key(salt, password):
    password = bytes(password, encoding="UTF-8")
    d = b''
    keyiv = b''
    for j in range(3):
        print("Derive DKEK share encryption key (Step " + str(j + 1) + " of 3)...")
        d = d + password + salt
        for i in range (10000000):
            d = hashlib.md5(d).digest()
        keyiv += d
    return keyiv


def encrypt_keyshare(keyshare, password):
    """
    Encrypt a DKEK keyshare with password
    :param keyshare: bytes
    :param password: UTF-8 str
    :return:
    """
    if len(keyshare) != 32:
        raise ValueError("Keyshare must be 32 bytes long!")
    salt = secrets.token_bytes(8)

    keyiv = _derive_dkek_share_key(salt, password)
    key = keyiv[:32]
    iv = keyiv[32:48]

    plain = keyshare + b"\x10" * 16
    ciphertext = AES.new(key, AES.MODE_CBC, iv).encrypt(plain)

    return b"Salted__" + salt + ciphertext


def decrypt_keyshare(keyshare, password):
    """
    Decrypt a DKEK share
    :param keyshare: bytes
    :param password: UTF-8 str
    :return: plain dkek share
    """
    if len(keyshare) != 64 or keyshare[:8] != b"Salted__":
        raise ValueError("Does not seem to be an encrypted key share!")
    salt = keyshare[8:16]

    keyiv = _derive_dkek_share_key(salt, password)
    key = keyiv[:32]
    iv = keyiv[32:48]

    plain = AES.new(key, AES.MODE_CBC, iv).decrypt(keyshare[16:])
    if not b"\x10" * 16 == plain[-16:]:
        raise ValueError("Decryption Failed! Wrong password?")

    return plain[:32]


def _test_dkek():
    """Round trip dkek encryption and decryption"""
    dkek = secrets.token_bytes(32)
    print(dkek)
    password = b"Password"
    enc = encrypt_keyshare(dkek, password)
    print(enc)
    plain = decrypt_keyshare(enc, password)
    print(plain)
    if dkek != plain:
        print("Doesn't match!")


class DKEK:
    def __init__(self):
        """Create an empty (all-zero) DKEK"""
        self.dkek = 0

    def as_bytes(self):
        return self.dkek.to_bytes(32, byteorder="big")

    def import_dkek_share(self, share):
        """Import a decrypted dkek share into this DKEK"""
        self.dkek ^= int.from_bytes(share, byteorder="big")

    def get_kcv(self):
        """
        Return the Key Check Value (KCV) of the internal DKEK
        """
        return hashlib.sha256(self.as_bytes()).digest()[:8]

    def get_kenc(self):
        """Derive the encryption key from the DKEK"""
        return hashlib.sha256(self.as_bytes() + b'\x00\x00\x00\x01').digest()

    def get_kmac(self):
        """Derive the message authentication key from the DKEK"""
        return hashlib.sha256(self.as_bytes() + b'\x00\x00\x00\x02').digest()

    def encode_key(self, private_key, public_key):
        """
        :param private_key: rsa privatekey
        :param public_key: rsa publickey
        :return: encrypted keyblob
        """
        assert(isinstance(private_key, rsa.PrivateKey))
        assert(isinstance(public_key, rsa.PublicKey))
        bb = self.get_kcv()

        mod = public_key.n
        use_crt = True
        if _get_number_of_bytes(mod) > 256:
            use_crt = False
        if use_crt:
            bb += struct.pack(">B", 6)
        else:
            bb += struct.pack(">B", 5)
        algo = b'\x04\x00\x7F\x00\x07\x02\x02\x02\x01\x02'  # "id-TA-RSA-v1-5-SHA-256" in some OID encoding...

        bb += struct.pack(">H", len(algo))
        bb += algo
        bb += b'\x00' * 6

        kb = secrets.token_bytes(8)
        # implement only RSA for now
        kb += struct.pack(">H", _get_number_of_bytes(mod) << 3)  # in bits, stored in 2 bytes
        if use_crt:
            # Key.CRT_DP1
            kb += struct.pack(">H", _get_number_of_bytes(private_key.exp1))
            kb += private_key.exp1.to_bytes(_get_number_of_bytes(private_key.exp1), "big")
            # Key.CRT_DQ1
            kb += struct.pack(">H", _get_number_of_bytes(private_key.exp2))
            kb += private_key.exp2.to_bytes(_get_number_of_bytes(private_key.exp2), "big")
            # p
            kb += struct.pack(">H", _get_number_of_bytes(private_key.p))
            kb += private_key.p.to_bytes(_get_number_of_bytes(private_key.p), "big")
            # Key.CRT_PQ = (Q^-1 mod P)
            kb += struct.pack(">H", _get_number_of_bytes(private_key.coef))
            kb += private_key.coef.to_bytes(_get_number_of_bytes(private_key.coef), "big")
            # q
            kb += struct.pack(">H", _get_number_of_bytes(private_key.q))
            kb += private_key.q.to_bytes(_get_number_of_bytes(private_key.q), "big")
        else:
            # Private Exponent = d
            kb += struct.pack(">H", _get_number_of_bytes(private_key.d))
            kb += private_key.d.to_bytes(_get_number_of_bytes(private_key.d), "big")

        kb += struct.pack(">H", _get_number_of_bytes(public_key.n))
        kb += public_key.n.to_bytes(_get_number_of_bytes(public_key.n), "big")
        kb += struct.pack(">H", _get_number_of_bytes(public_key.e))
        kb += public_key.e.to_bytes(_get_number_of_bytes(public_key.e), "big")

        # pad to 16 byte block with first padding char being \x80
        if len(kb) % 16 != 0:
            kb += b'\x80'
        while len(kb) % 16 != 0:
            kb += b'\x00'

        plain = kb
        iv = b'\x00' * 16
        kenc = self.get_kenc()
        cipher = AES.new(kenc, AES.MODE_CBC, iv).encrypt(plain)
        bb += cipher
        kmac = self.get_kmac()
        bb += CMAC.new(kmac, ciphermod=AES).update(bb).digest()
        #self.dump_keyblob(bb)
        return bb

    def dump_keyblob(self, keyblob):
        """
        Decrypt and anaylze an encrypted keyblob
        :param keyblob: encrypted keyblob
        """
        cobj = CMAC.new(self.get_kmac(), ciphermod=AES)
        cobj.update(keyblob[:-16])
        try:
            cobj.verify(keyblob[-16:])
            macok = True
        except ValueError:
            macok = False

        keytype = keyblob[8]
        print("Values from key blob:")
        print("---------------------")
        print("MAC OK:              " + str(macok))
        print("KCV:                 " + keyblob[:8].hex())
        print("Key Type:            " + str(keytype) + "    [5=RSA, 6=RSA-CRT, 12=ECC, 15=AES]")

        offset = 9
        length = struct.unpack_from(">H", keyblob, offset=offset)[0]

        # no idea how to decode OIDs...
        print("Default Algorithm ID : " + keyblob[offset + 2:offset+2+length].hex()
              + " (" + str(length) + ")     [Default algorithm]")

        offset += length + 2
        length = struct.unpack_from(">H", keyblob, offset=offset)[0]
        print("Allowed Algorithm IDs : " + keyblob[offset + 2:offset+2+length].hex() + " (" + str(length) + ")")

        offset += length + 2
        length = struct.unpack_from(">H", keyblob, offset=offset)[0]
        print("Access Conditions   : " + keyblob[offset + 2:offset+2+length].hex() + " (" + str(length) + ")")

        offset += length + 2
        length = struct.unpack_from(">H", keyblob, offset=offset)[0]
        print("Access OID   : " + keyblob[offset + 2:offset+2+length].hex() + " (" + str(length) + ")")

        offset += length + 2

        # Decrypt Key data
        iv = b'\x00' * 16
        plainkey = AES.new(self.get_kenc(), AES.MODE_CBC, iv).decrypt(keyblob[offset:len(keyblob) - 16])
        # print(plainkey.hex())

        print("Randomize             : " + plainkey[:8].hex() + "    [Random data prepended at export]")
        keysize = struct.unpack_from(">H", plainkey, offset=8)[0]
        print("Key size              : " + str(keysize) + "    [Key size in bits (ECC/RSA) or bytes (AES)]")

        offset = 10
        if keytype == 5:
            length = struct.unpack_from(">H", plainkey, offset=offset)[0]
            print("Private Exponent      : " + plainkey[offset + 2:offset + 2 + length].hex() + " (" + str(length) + ")")
            offset += 2 + length

            length = struct.unpack_from(">H", plainkey, offset=offset)[0]
            print("Modulus      : " + plainkey[offset + 2:offset + 2 + length].hex() + " (" + str(length) + ")")
            offset += 2 + length

            length = struct.unpack_from(">H", plainkey, offset=offset)[0]
            print("Public Exponent      : " + plainkey[offset + 2:offset + 2 + length].hex() + " (" + str(length) + ")")
            offset += 2 + length

        elif keytype == 6:
            length = struct.unpack_from(">H", plainkey, offset=offset)[0]
            print("DP1 = d mod (p^-1)      : " + plainkey[offset + 2:offset + 2 + length].hex() + " (" + str(length) + ")")
            offset += 2 + length

            length = struct.unpack_from(">H", plainkey, offset=offset)[0]
            print("DQ1 = d mod (q^-1)      : " + plainkey[offset + 2:offset + 2 + length].hex() + " (" + str(length) + ")")
            offset += 2 + length

            length = struct.unpack_from(">H", plainkey, offset=offset)[0]
            print("Prime factor p      : " + plainkey[offset + 2:offset + 2 + length].hex() + " (" + str(length) + ")")
            offset += 2 + length

            length = struct.unpack_from(">H", plainkey, offset=offset)[0]
            print("PQ = q^-1 mod p      : " + plainkey[offset + 2:offset + 2 + length].hex() + " (" + str(length) + ")")
            offset += 2 + length

            length = struct.unpack_from(">H", plainkey, offset=offset)[0]
            print("Prime factor q     : " + plainkey[offset + 2:offset + 2 + length].hex() + " (" + str(length) + ")")
            offset += 2 + length

            length = struct.unpack_from(">H", plainkey, offset=offset)[0]
            print("Modulus     : " + plainkey[offset + 2:offset + 2 + length].hex() + " (" + str(length) + ")")
            offset += 2 + length

            length = struct.unpack_from(">H", plainkey, offset=offset)[0]
            print("Public Exponent     : " + plainkey[offset + 2:offset + 2 + length].hex() + " (" + str(length) + ")")
        else:
            print("Unknown Key Type")