# Nitrokeywrapper
# Copyright (c) 2020 Marcus Hoffmann <bubu@bubu1.eu>
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, see <http://www.gnu.org/licenses/>.

from asn1crypto.core import Sequence, OctetString, BitString, UTF8String, Integer


class KeyLabel(Sequence):
    _fields = [
        ('label', UTF8String),
    ]


class KeyID(Sequence):
    _fields = [
        ('keyid', OctetString),
        ('const_data', BitString),
    ]


class MyEmptySequence(Sequence):
    _fields = [
        ('empty', OctetString),
    ]


class ModulusSize(Sequence):
    _fields = [
        ('another_sequence', MyEmptySequence),
        ('modulus_size', Integer),
    ]


class PrKD(Sequence):
    _fields = [
        ('keylabel', KeyLabel),
        ('keyid', KeyID),
        ('modulus_size', Sequence, {'explicit': 1}),
    ]


class WrappedKey(Sequence):
    _fields = [
        ('encrypted_key', OctetString),
        ('PrKD', PrKD),  # PKCS#15 PrivateKeyDescription
        ('cert', Sequence),
    ]


def build_prkd_for_rsa(keyid, label, modulussize):
    keylabel = KeyLabel()
    keylabel['label'] = UTF8String(label)

    keyidseq = KeyID()
    keyidseq['keyid'] = OctetString(keyid.to_bytes(1, "big"))
    keyidseq['const_data'] = BitString((0, 1, 1, 1, 0, 1))

    modulus_size_seq = ModulusSize()
    my_empty_sequence = MyEmptySequence()
    my_empty_sequence['empty'] = OctetString(b'')
    modulus_size_seq['another_sequence'] = my_empty_sequence
    modulus_size_seq['modulus_size'] = Integer(modulussize)

    prkd = PrKD()
    prkd['keylabel'] = keylabel
    prkd['keyid'] = keyidseq
    prkd['modulus_size'] = modulus_size_seq
    return prkd


def build_wrapped_key(encoded_key, bitsize, alias, cert, keyid=255):
    """

    :param encoded_key: DKEK encoded rsa privatekeyblob
    :param bitsize: size of rsa key in bits
    :param alias: keyalias from keystore
    :param cert: DER encoded X.509 cert in bytes
    :param keyid: The keyid the key will be unwrapped into
    :return: DER encoded WrappedKey ASN1 structure as bytes
    """
    wrapped_key = WrappedKey()
    wrapped_key['encrypted_key'] = encoded_key
    wrapped_key['PrKD'] = build_prkd_for_rsa(keyid, alias, bitsize)
    wrapped_key['cert'] = Sequence.load(cert)
    return wrapped_key.dump()
