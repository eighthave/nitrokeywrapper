## Nitrokey HSM Keywrapper

#### About

This tool can wrap all RSA keys inside a java jks keystore for usage with a [NitroKey](https://nitrokey.com) HSM2.
They can be imported with `sc-hsm-tool` from the OpenSC project or [SmartCardShell](https://www.openscdp.org/scsh3/download.html).

ECC keys are currently not supported but support for them could be addded.

#### Usage

* Install all dependencies from `requirements.txt`
* set up https://github.com/OpenSC/OpenSC/wiki/SmartCardHSM#using-key-backup-and-restore
* Edit keywrapper.py and enter `JKS_PWD`, `JKS_PATH`, `NUMBER_OF_DKEK_SHARES`, `DKEK_PWDS` and `DKEK_SHAREPATHS`
* Run `./keywrapper.py`

Afterwards you can import the keys with `sc-hsm-tool`:
Example import:
```
sc-hsm-tool --pin XXXXXX --unwrap-key alias_wrapped.bin --key-reference 1 --force
```  
The `--force` parameter will overwrite the key that's currently sitting at the specified key-reference as well as any associated certificates from previous import attempts.

#### References/Acknowledgements

This project mostly reimplements the functions from `DKEK.js`  as well as `SmartCardHSM.js:buildPrkDforRSA()` from [SmartCardShell](https://www.openscdp.org/scsh3/download.html) but without the need for a java, javascript or GUI environment.

The [sc-hsm-tool](https://github.com/OpenSC/OpenSC/blob/master/src/tools/sc-hsm-tool.c) source code was a helpful reference as well.